package org.kanan;

import java.util.Arrays;

public class App 
{
    public static void main( String[] args )
    {
        if (args.length == 0) {
            System.out.println("No arguments provided.");
            return;
        }

        if (args.length > 10) {
            System.out.println("Exceeded maximum number of arguments. Only up to 10 arguments allowed.");
            return;
        }

        int[] numbers = new int[args.length];
        for (int i = 0; i < args.length; i++) {
            try {
                numbers[i] = Integer.parseInt(args[i]);
            } catch (NumberFormatException e) {
                System.out.println("Invalid argument: " + args[i]);
                return;
            }
        }

        Arrays.sort(numbers);

        System.out.println("Sorted numbers:");
        for (int num : numbers) {
            System.out.println(num);
        }

    }
}
