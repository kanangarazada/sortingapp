package org.kanan;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.util.Arrays;
import java.util.Collection;

import static org.junit.Assert.assertArrayEquals;

@RunWith(Parameterized.class)
public class SortingAppParameterizedTest {
    private final String[] args;
    private final String expectedOutput;
    private final ByteArrayOutputStream outputStream = new ByteArrayOutputStream();

    @Before
    public void setUp() {
        System.setOut(new PrintStream(outputStream));
    }

    public SortingAppParameterizedTest(String[] args, String expectedOutput) {
        this.args = args;
        this.expectedOutput = expectedOutput;
    }

    @Parameterized.Parameters
    public static Collection<Object[]> data() {
        return Arrays.asList(new Object[][] {
                { new String[]{}, "No arguments provided." },
                { new String[]{"5"}, "Sorted numbers:\n5" },
                { new String[]{"9", "7", "3", "5", "1", "2", "8", "6", "4", "0"}, "Sorted numbers:\n0\n1\n2\n3\n4\n5\n6\n7\n8\n9" },
                { new String[]{"10", "20", "30"}, "Sorted numbers:\n10\n20\n30" },
                { new String[]{"-5", "-10", "0"}, "Sorted numbers:\n-10\n-5\n0" },
                { new String[]{"-1", "0", "1"}, "Sorted numbers:\n-1\n0\n1" },
                { new String[]{"100", "200", "150", "50"}, "Sorted numbers:\n50\n100\n150\n200" },
                { new String[]{"7", "7", "7", "7"}, "Sorted numbers:\n7\n7\n7\n7" },
                { new String[]{"3", "2", "1", "4", "5"}, "Sorted numbers:\n1\n2\n3\n4\n5" },
                { new String[]{"9", "7", "3", "5", "1", "2", "8", "6", "4", "0", "11", "10", "13", "12"}, "Exceeded maximum number of arguments. Only up to 10 arguments allowed." }
        });
    }

    @Test
    public void testSortingApp() {
        App.main(args);
        String[] expectedLines = expectedOutput.split("\\r?\\n");
        String[] actualLines = outputStream.toString().trim().split("\\r?\\n");
        assertArrayEquals(expectedLines, actualLines);
    }
}