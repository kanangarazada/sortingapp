package org.kanan;

import org.junit.Before;
import org.junit.Test;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

import static org.junit.Assert.*;

public class AppTest 
{
    private final ByteArrayOutputStream outputStream = new ByteArrayOutputStream();

    @Before
    public void setUp() {
        System.setOut(new PrintStream(outputStream));
    }
    @Test
    public void shouldAnswerWithTrue()
    {
        String[] args = {};
        App.main(args);
        assertEquals("No arguments provided.", outputStream.toString().trim());
    }

    @Test
    public void testSortingWithOneArgument() {
        String[] args = {"5"};
        App.main(args);
        String[] expectedLines = {
                "Sorted numbers:",
                "5"
        };
        String[] actualLines = outputStream.toString().trim().split("\\r?\\n");
        assertArrayEquals(expectedLines, actualLines);
    }

    @Test
    public void testSortingWithTenArguments() {
        String[] args = {"9", "7", "3", "5", "1", "2", "8", "6", "4", "0"};
        App.main(args);
        String[] expectedLines = {
                "Sorted numbers:",
                "0",
                "1",
                "2",
                "3",
                "4",
                "5",
                "6",
                "7",
                "8",
                "9"
        };
        String[] actualLines = outputStream.toString().trim().split("\\r?\\n");
        assertArrayEquals(expectedLines, actualLines);
    }

    @Test
    public void testSortingWithMoreThanTenArguments() {
        String[] args = {"9", "7", "3", "5", "1", "2", "8", "6", "4", "0", "10", "11", "13"};
        App.main(args);
        // Add assertions to check the expected output
        assertEquals("Exceeded maximum number of arguments. Only up to 10 arguments allowed.", outputStream.toString().trim());
    }
}
